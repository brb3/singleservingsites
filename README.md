# Single-serving Sites
![Built With Resentment][resentment] ![Uses Badges][badges] ![Made With Crayons][crayons]

An unofficial, incomplete, haphazardly curated list of single-serving sites

### Sites

* [20 Limit][20limit]
* [Am I Awesome?][amiawesome]
* [Arbitrary Awards][arbawards]
* [Big Ass Message][bigmessage]
* [Butt Generator][buttgenerator]
* [CensorBleep][censor]
* [Claim of the Day][claimofday]
* [Click Click Click][ccc]
* [Copychar][copychar]
* [Corntab][corntab]
* [Corrupt My File][corruptfile]
* [Days Since NPM Broke][npmbroke]
* [D-E-F-I-N-I-T-E-L-Y][definitely]
* [Disclaimerfy][disclaimer]
* [Do People Scroll?][dopeoplescroll]
* [Do Nothing For Two Minutes][donothing]
* [Do Websites Need To Look The Same In Every Browser?][everybrowser]
* [Does My Site Need HTTPS?][needhttps]
* [Does The Mail Run Today][mailrun]
* [Down For Everyone Or Just Me?][downeveryone]
* [Dribbble Comments Generator][dribbblecomments]
* [Egg Timer][eggtimer]
* [Fast Good Cheap][fastgoodcheap]
* [File Pizza][filepizza]
* [Flipping Typical][flipping]
* [Fucking Google It][googleit]
* [Get My IP Address][getip]
* [Hardcore Prawn Lawn][hardcoreprawnlawn]
* [Has the Large Hadron Collider Destroyed The World Yet?][hadron]
* [Heat Of The Moment - Asia][heatmoment]
* [Hex Clock][hexclock]
* [How Do You Have A Party In Space][hdyhapis]
* [How Many People Are In Space Right Now][hmpaisrn]
* [HTML5 Zombo][zombo]
* [ifconfig][ifconfig]
* [IIIIIIII][iii]
* [Illegal Tender Terms of Service][moneylicense]
* [Infinite Wacky Waving][wackywaving]
* [Instant Rimshot][rimshot]
* [Internet Down][internetdown]
* [Internet First Page][firstpage]
* [Internet Last Page][lastpage]
* [Internet Rules][rules]
* [It Will Never Be The Same][neversame]
* [Is California On Fire?][califire]
* [Is DMX In Jail?][dmxjail]
* [Is It April Fools?][aprilfools]
* [Is It Christmas?][christmas]
* [Is It Time For A Nap?][naptime]
* [Is Mr. Brightside Still In The Charts?][brightside]
* [Is Pizza Half Price?][pizzahalfprice]
* [Is Riverbend Over Yet?][riverbend]
* [Is The Dow Up?][dowup]
* [Is The L Train Fucked][ltrain]
* [Just Flip A Coin][coinflip]
* [LaLaLaaa][lala]
* [Let Me Google That For You][lmgtfy]
* [Lets Turn This Fucking Website Yellow][yellow]
* [Life Is Not Fair][notfair]
* [Material Palette][materialpalette]
* [Motherfucking Website][mfwebsite]
* [Namech_k][namechk]
* [NGINX Config Generator][nginx]
* [Noisli][noisli]
* [Nooooooooooooooo][no]
* [No Time For Love Dr. Jones][notime]
* [Passove Aggressive Password Machine][papm]
* [PDF.to][pdfto]
* [Percentage Calculator][percentcalc]
* [Periodic Table of Elements][elements]
* [Poop.bike][poopbike]
* [Python Is Dead - Science][pythondead]
* [Raquo][raquo]
* [Regexr][regexr]
* [Ruby Is Dead - Science][rubydead]
* [Sad Trombone][trombone]
* [Scoreboardz][scoreboardz]
* [Send a Message To][messageto]
* [SharkLasers][sharklasers]
* [Sketchy Website][sketchy]
* [Steamroller][steamroller]
* [Should I Bring An Umbrella?][umbrella]
* [Should I Do It?][doit]
* [Sometimes Red Sometimes Blue][redblue]
* [Swear Jar][swearjar]
* [The Daily Nice][dailynice]
* [The Best Dinosaur][bestdino]
* [uiGradients][uigradients]
* [Web Safe Colors][websafecolors]
* [What Is My Browser?][browserinfo]
* [What Is My OS?][osinfo]
* [What Is That In Danzigs?][danzig]
* [What Rhymes With?][rhymes]
* [What Week Is It?][whatweek]
* [When Is Easter?][easter]
* [When is the next Steam Sale][whenisthenextsteamsale]
* [Wikileaks Or Whatever][wikiwhat]
* [Wilhelm Scream][wilhelm]
* [You Fell Asleep Watching A DVD][fellasleep]

### Todos

* Consider if there is a better way to sort these?
* Add NSFW tags where applicable
* Add noise warning tags where applicable
* Consider pruning the list
* Consider adding a submission form


### [Contributing][contributing]

Check the [contributing guidelines][contributing] before proceeding:

* Fork it: `https://gitlab.com/ryanmaynard/singleservingsites/forks/new`
* Make a new branch `git checkout -b my-new-feature`
* Commit your changes `git commit -am 'Add some feature'`
* Push to the branch `git push origin my-new-feature`
* Submit a new Pull Request `https://gitlab.com/ryanmaynard/singleservingsites/merge_requests/new`


### [License][license]

This project is under an [MIT][mittldr] license.

[resentment]: https://forthebadge.com/images/badges/built-with-resentment.svg
[badges]: https://forthebadge.com/images/badges/uses-badges.svg
[crayons]: https://forthebadge.com/images/badges/made-with-crayons.svg

[20limit]: http://www.20limit.com/
[amiawesome]: http://amiawesome.com/
[arbawards]: http://arbitraryawards.com/
[bigmessage]: https://bigassmessage.com/
[buttgenerator]: http://www.buttgenerator.com/
[censor]: https://www.censorbleep.com/
[claimofday]: https://claim-of-the-day.de/
[ccc]: https://clickclickclick.click/
[copychar]: https://copychar.cc/
[corntab]: http://corntab.com/
[corruptfile]: http://www.corruptmyfile.com/
[npmbroke]: https://www.dayssincenpmbroke.com/#
[definitely]: http://www.d-e-f-i-n-i-t-e-l-y.com/
[disclaimer]: http://disclaimerfy.com/
[needhttps]: https://doesmysiteneedhttps.com/
[mailrun]: http://www.doesthemailruntoday.com/
[downeveryone]: https://downforeveryoneorjustme.com/
[dopeoplescroll]: http://dopeoplescroll.com/
[donothing]: http://www.donothingfor2minutes.com/
[everybrowser]: http://dowebsitesneedtolookexactlythesameineverybrowser.com/
[dribbblecomments]: http://commments.com/
[eggtimer]: http://e.ggtimer.com/
[fastgoodcheap]: https://fastgood.cheap/
[filepizza]: https://file.pizza/
[flipping]: http://flippingtypical.com/
[googleit]: http://www.fuckinggoogleit.com/
[getip]: http://www.getmyipaddress.info/
[hardcoreprawnlawn]: http://www.hardcoreprawnlawn.com/
[hadron]: http://www.hasthelargehadroncolliderdestroyedtheworldyet.com/
[heatmoment]: http://www.heatofthemoment.asia/
[hexclock]: http://www.jacopocolo.com/hexclock/
[hdyhapis]: https://howdoyouhaveapartyin.space/
[hmpaisrn]: https://www.howmanypeopleareinspacerightnow.com/
[zombo]: https://html5zombo.com/
[ifconfig]: https://ifconfig.me
[iii]: http://www.iiiiiiii.com/
[moneylicense]: http://www.moneylicense.com/
[wackywaving]: http://infinitewackywaving.com/
[rimshot]: http://instantrimshot.com/
[internetdown]: https://www.internetdown.info/
[firstpage]: http://www.internetfirstpage.com/ 
[lastpage]: http://www.internetlastpage.com/
[rules]: http://www.internetrules.info/
[neversame]: http://www.itwillneverbethesame.com/
[califire]: http://www.iscaliforniaonfire.com/
[dmxjail]: http://isdmxinjail.com/
[aprilfools]: http://isitaprilfools.com/
[coinflip]: https://justflipacoin.com/
[lala]: http://www.lalalaa.com/
[lmgtfy]: http://lmgtfy.com/
[yellow]: http://www.letsturnthisfuckingwebsiteyellow.com/
[notfair]: http://www.lifeisnotfair.org/
[christmas]: https://isitchristmas.com/
[naptime]: http://isittimeforanap.com/
[brightside]: http://ismrbrightsidestillinthecharts.com/
[pizzahalfprice]: http://ispizzahalfprice.com
[riverbend]: https://www.isriverbendoveryet.com/
[dowup]: http://www.isthedowup.com/
[ltrain]: http://www.istheltrainfucked.com/
[materialpalette]: https://www.materialpalette.com/
[mfwebsite]: https://motherfuckingwebsite.com/
[namechk]: https://namechk.com/
[nginx]: https://nginxconfig.io/
[noisli]: https://www.noisli.com/
[no]: http://www.nooooooooooooooo.com/
[notime]: http://www.notimeforlovedrjones.com/
[papm]: https://trypap.com/
[pdfto]:https://pdf.to/ 
[percentcalc]: http://www.percentage-calculator.info/
[elements]: https://periodictableofchemicalelements.com/
[poopbike]: http://poop.bike
[pythondead]: https://pythonisdead.science
[raquo]: http://www.raquo.net/
[regexr]: https://regexr.com/
[rubydead]: https://rubyisdead.science
[trombone]: https://sadtrombone.com/
[scoreboardz]: http://www.scoreboardz.com/
[messageto]: http://sendamessage.to/
[sharklasers]: https://www.sharklasers.com/
[sketchy]: https://www.sketchywebsite.net/
[steamroller]: http://www.steamroller.com/
[umbrella]: http://shouldibringanumbrella.com/
[doit]: http://yesyesno.me/
[redblue]: http://www.sometimesredsometimesblue.com/
[swearjar]: http://www.fuck-button.com/
[dailynice]: http://www.thedailynice.com/
[bestdino]: http://www.thebestdinosaur.com/
[uigradients]: https://uigradients.com/
[websafecolors]: http://websafecolors.info/
[browserinfo]: http://www.whatismybrowser.info/
[osinfo]: http://www.whatismyos.info/
[danzig]: http://hudsonhongo.com/danzig/
[rhymes]: https://www.whatrhymeswith.info/
[whatweek]: https://whatweekisit.com/
[easter]: http://www.when-is-easter.info/
[whenisthenextsteamsale]: https://www.whenisthenextsteamsale.com/
[wikiwhat]: http://wikileaksorwhatever.com
[wilhelm]: http://www.wilhelmscream.net/
[fellasleep]: http://www.youfellasleepwatchingadvd.com/

[contributing]: ./CONTRIBUTING.md
[license]: ./LICENSE
[mittldr]: https://tldrlegal.com/license/mit-license
